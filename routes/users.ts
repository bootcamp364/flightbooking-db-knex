import { Router } from 'express';
import userController from '../controller/users';

const router = Router();

router.get('/', userController.get_all_users);
router.post('/', userController.create_new_user);
router.put('/:userId', userController.update_user_by_id);
router.delete('/:userId', userController.delete_user_by_id);

// app.get('/users', (req, res) => {
//   knex.raw('select * from users').then((users) => res.send(users.rows));
// });

export default router;
