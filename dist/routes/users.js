"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var users_1 = __importDefault(require("../controller/users"));
var router = (0, express_1.Router)();
router.get('/', users_1.default.get_all_users);
router.post('/', users_1.default.create_new_user);
router.put('/:userId', users_1.default.update_user_by_id);
router.delete('/:userId', users_1.default.delete_user_by_id);
// app.get('/users', (req, res) => {
//   knex.raw('select * from users').then((users) => res.send(users.rows));
// });
exports.default = router;
