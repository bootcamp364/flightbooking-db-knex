"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var users_1 = __importDefault(require("./routes/users"));
var port = process.env.PORT || 8000;
var app = (0, express_1.default)();
app.use(express_1.default.json());
app.use('/user', users_1.default);
app.listen(port, function () {
    console.log('listening on port: ', port);
});
