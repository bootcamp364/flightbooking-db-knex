var config = {
    client: 'pg',
    version: '7.2',
    connection: {
        host: 'localhost',
        user: 'postgres',
        password: 'tani123',
        database: 'dummy',
    },
    migrations: {
        directory: __dirname + '/db/migrations',
    },
    seeds: {
        directory: __dirname + '/db/seeds',
    },
};
module.exports = require('knex')(config);
