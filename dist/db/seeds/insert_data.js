var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var faker = require('@faker-js/faker').faker;
/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
var NUM = 1000;
function generateRandomNumber(min, max) {
    if (min === void 0) { min = 5; }
    if (max === void 0) { max = NUM; }
    return Math.floor(Math.random() * (max - min)) + min;
}
var createFakeUsers = function () { return ({
    phonenumber: faker.phone.number('+91 ##### #####'),
    email: faker.internet.email(),
    password: faker.internet.password(),
}); };
var createFakeTravelers = function () { return ({
    fname: faker.name.firstName(),
    mname: faker.name.middleName(),
    lname: faker.name.lastName(),
    gender: faker.helpers.arrayElement(['male', 'female']),
}); };
var createFakeAirlines = function () { return ({
    airline_name: faker.lorem.word(),
}); };
var createFakeFlights = function () { return ({
    airline_id: generateRandomNumber(5, 800),
    flight_number: faker.address.buildingNumber(),
}); };
var createFakeRoutes = function () { return ({
    from_place: faker.address.city(),
    to_place: faker.address.city(),
    from_time: faker.datatype.datetime(),
    duration: generateRandomNumber(1, 100),
    flight_id: generateRandomNumber(5, 800),
}); };
var createFakeBookings = function () { return ({
    userbooked_id: generateRandomNumber(5, 800),
    route_id: generateRandomNumber(5, 800),
    price: faker.commerce.price(),
    meal_add_on: faker.lorem.slug(),
    luggage_add_on: faker.lorem.slug(),
    fare_type: faker.helpers.arrayElement(['std', 'flexi', 'refundable']),
    no_of_seats: generateRandomNumber(1, 10),
}); };
var createFakeClasses = function () { return ({
    type: faker.helpers.arrayElement(['eco', 'premium', 'business']),
    cancellation_price: faker.commerce.price(),
}); };
var createFakeSeats = function () { return ({
    seat_number: faker.lorem.word(5),
    class_id: generateRandomNumber(5, 800),
    flight_id: generateRandomNumber(5, 800),
}); };
var createFakeTickets = function () { return ({
    booking_id: generateRandomNumber(5, 800),
    traveler_id: generateRandomNumber(5, 800),
    seat_id: generateRandomNumber(5, 800),
    category: faker.helpers.arrayElement(['adult', 'child', 'infant']),
    route_id: generateRandomNumber(5, 800),
}); };
exports.seed = function (knex) {
    return __awaiter(this, void 0, void 0, function () {
        var fakeUsers, i, fakeTravelers, i, fakeAirlines, i, fakeFlights, i, fakeRoutes, i, fakeBookings, i, fakeClasses, i, fakeSeats, i, fakeTickets, i;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: 
                // Deletes ALL existing entries
                return [4 /*yield*/, knex('users').del()];
                case 1:
                    // Deletes ALL existing entries
                    _a.sent();
                    return [4 /*yield*/, knex('travelers').del()];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, knex('airlines').del()];
                case 3:
                    _a.sent();
                    return [4 /*yield*/, knex('flights').del()];
                case 4:
                    _a.sent();
                    return [4 /*yield*/, knex('routes').del()];
                case 5:
                    _a.sent();
                    return [4 /*yield*/, knex('bookings').del()];
                case 6:
                    _a.sent();
                    return [4 /*yield*/, knex('classes').del()];
                case 7:
                    _a.sent();
                    return [4 /*yield*/, knex('seats').del()];
                case 8:
                    _a.sent();
                    return [4 /*yield*/, knex('tickets').del()];
                case 9:
                    _a.sent();
                    fakeUsers = [];
                    for (i = 0; i < NUM; i++) {
                        fakeUsers.push(createFakeUsers());
                    }
                    return [4 /*yield*/, knex('users').insert(fakeUsers)];
                case 10:
                    _a.sent();
                    fakeTravelers = [];
                    for (i = 0; i < NUM; i++) {
                        fakeTravelers.push(createFakeTravelers());
                    }
                    return [4 /*yield*/, knex('travelers').insert(fakeTravelers)];
                case 11:
                    _a.sent();
                    fakeAirlines = [];
                    for (i = 0; i < NUM; i++) {
                        fakeAirlines.push(createFakeAirlines());
                    }
                    return [4 /*yield*/, knex('airlines').insert(fakeAirlines)];
                case 12:
                    _a.sent();
                    fakeFlights = [];
                    for (i = 0; i < NUM; i++) {
                        fakeFlights.push(createFakeFlights());
                    }
                    return [4 /*yield*/, knex('flights').insert(fakeFlights)];
                case 13:
                    _a.sent();
                    fakeRoutes = [];
                    for (i = 0; i < NUM; i++) {
                        fakeRoutes.push(createFakeRoutes());
                    }
                    return [4 /*yield*/, knex('routes').insert(fakeRoutes)];
                case 14:
                    _a.sent();
                    fakeBookings = [];
                    for (i = 0; i < NUM; i++) {
                        fakeBookings.push(createFakeBookings());
                    }
                    return [4 /*yield*/, knex('bookings').insert(fakeBookings)];
                case 15:
                    _a.sent();
                    fakeClasses = [];
                    for (i = 0; i < NUM; i++) {
                        fakeClasses.push(createFakeClasses());
                    }
                    return [4 /*yield*/, knex('classes').insert(fakeClasses)];
                case 16:
                    _a.sent();
                    fakeSeats = [];
                    for (i = 0; i < NUM; i++) {
                        fakeSeats.push(createFakeSeats());
                    }
                    return [4 /*yield*/, knex('seats').insert(fakeSeats)];
                case 17:
                    _a.sent();
                    fakeTickets = [];
                    for (i = 0; i < NUM; i++) {
                        fakeTickets.push(createFakeTickets());
                    }
                    return [4 /*yield*/, knex('tickets').insert(fakeTickets)];
                case 18:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
};
