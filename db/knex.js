var config = {
  client: 'pg',
  version: '7.2',
  connection: {
    host: 'db',
    user: 'docker',
    password: 'tani123',
    database: 'docker',
  },
  migrations: {
    directory: __dirname + '/migrations',
  },
  seeds: {
    directory: __dirname + '/seeds',
  },
};
module.exports = require('knex')(config);
