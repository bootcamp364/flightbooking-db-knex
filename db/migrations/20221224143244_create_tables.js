/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema
    .createTable('users', (table) => {
      table.increments('id');
      table.string('phonenumber', 255).notNullable();
      table.string('email', 255).notNullable();
      table.string('password', 255).notNullable();
    })
    .createTable('travelers', (table) => {
      table.increments('id');
      table.string('fname').notNullable();
      table.string('mname');
      table.string('lname').notNullable();
      table.enu('gender', ['male', 'female']).notNullable();
    })
    .createTable('airlines', (table) => {
      table.increments('id');
      table.string('airline_name', 255);
    })
    .createTable('flights', (table) => {
      table.increments('id');
      table
        .integer('airline_id')
        .notNullable()
        .references('id')
        .inTable('airlines');
      table.string('flight_number').notNullable();
    })
    .createTable('routes', (table) => {
      table.increments('id');
      table.string('from_place').notNullable();
      table.string('to_place').notNullable();
      table.timestamp('from_time').notNullable();
      table.integer('duration').notNullable();
      table
        .integer('flight_id')
        .notNullable()
        .references('id')
        .inTable('flights');
    })
    .createTable('bookings', (table) => {
      table.increments('id');
      table
        .integer('userbooked_id')
        .notNullable()
        .references('id')
        .inTable('users');
      table
        .integer('route_id')
        .notNullable()
        .references('id')
        .inTable('routes');
      table.float('price').notNullable();
      table.string('meal_add_on').defaultTo(null);
      table.string('luggage_add_on').defaultTo(null);
      table.enu('fare_type', ['std', 'flexi', 'refundable']).notNullable();
      table.integer('no_of_seats').notNullable().defaultTo(1);
    })
    .createTable('classes', (table) => {
      table.increments('id');
      table.enu('type', ['eco', 'premium', 'business']).notNullable();
      table.float('cancellation_price').notNullable();
    })
    .createTable('seats', (table) => {
      table.increments('id');
      table.string('seat_number', 255).notNullable();
      table
        .integer('class_id')
        .notNullable()
        .references('id')
        .inTable('classes');
      table
        .integer('flight_id')
        .notNullable()
        .references('id')
        .inTable('flights');
    })
    .createTable('tickets', (table) => {
      table.increments('id');
      table
        .integer('booking_id')
        .notNullable()
        .references('id')
        .inTable('bookings');
      table
        .integer('traveler_id')
        .notNullable()
        .references('id')
        .inTable('travelers');
      table.integer('seat_id').notNullable().references('id').inTable('seats');
      table.enu('category', ['adult', 'child', 'infant']).notNullable();
      table
        .integer('route_id')
        .notNullable()
        .references('id')
        .inTable('routes');
    });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema
    .dropTable('tickets')
    .dropTable('travelers')
    .dropTable('bookings')
    .dropTable('users')

    .dropTable('routes')
    .dropTable('seats')

    .dropTable('flights')
    .dropTable('airlines')
    .dropTable('classes');
};
