const { faker } = require('@faker-js/faker');

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */

const NUM = 1000;

function generateRandomNumber(min = 5, max = NUM) {
  return Math.floor(Math.random() * (max - min)) + min;
}

const createFakeUsers = () => ({
  phonenumber: faker.phone.number('+91 ##### #####'),
  email: faker.internet.email(),
  password: faker.internet.password(),
});

const createFakeTravelers = () => ({
  fname: faker.name.firstName(),
  mname: faker.name.middleName(),
  lname: faker.name.lastName(),
  gender: faker.helpers.arrayElement(['male', 'female']),
});

const createFakeAirlines = () => ({
  airline_name: faker.lorem.word(),
});

const createFakeFlights = () => ({
  airline_id: generateRandomNumber(5, 800),
  flight_number: faker.address.buildingNumber(),
});

const createFakeRoutes = () => ({
  from_place: faker.address.city(),
  to_place: faker.address.city(),
  from_time: faker.datatype.datetime(),
  duration: generateRandomNumber(1, 100),
  flight_id: generateRandomNumber(5, 800),
});

const createFakeBookings = () => ({
  userbooked_id: generateRandomNumber(5, 800),
  route_id: generateRandomNumber(5, 800),
  price: faker.commerce.price(),
  meal_add_on: faker.lorem.slug(),
  luggage_add_on: faker.lorem.slug(),
  fare_type: faker.helpers.arrayElement(['std', 'flexi', 'refundable']),
  no_of_seats: generateRandomNumber(1, 10),
});

const createFakeClasses = () => ({
  type: faker.helpers.arrayElement(['eco', 'premium', 'business']),
  cancellation_price: faker.commerce.price(),
});

const createFakeSeats = () => ({
  seat_number: faker.lorem.word(5),
  class_id: generateRandomNumber(5, 800),
  flight_id: generateRandomNumber(5, 800),
});

const createFakeTickets = () => ({
  booking_id: generateRandomNumber(5, 800),
  traveler_id: generateRandomNumber(5, 800),
  seat_id: generateRandomNumber(5, 800),
  category: faker.helpers.arrayElement(['adult', 'child', 'infant']),
  route_id: generateRandomNumber(5, 800),
});

exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('users').del();
  await knex('travelers').del();
  await knex('airlines').del();
  await knex('flights').del();
  await knex('routes').del();
  await knex('bookings').del();
  await knex('classes').del();
  await knex('seats').del();
  await knex('tickets').del();

  // Initialize constants

  // USERS TABLE
  const fakeUsers = [];
  for (let i = 0; i < NUM; i++) {
    fakeUsers.push(createFakeUsers());
  }
  await knex('users').insert(fakeUsers);

  // TRAVELERS TABLE
  const fakeTravelers = [];
  for (let i = 0; i < NUM; i++) {
    fakeTravelers.push(createFakeTravelers());
  }
  await knex('travelers').insert(fakeTravelers);

  // AIRLINES TABLE
  const fakeAirlines = [];
  for (let i = 0; i < NUM; i++) {
    fakeAirlines.push(createFakeAirlines());
  }
  await knex('airlines').insert(fakeAirlines);

  // FLIGHTS TABLE
  const fakeFlights = [];
  for (let i = 0; i < NUM; i++) {
    fakeFlights.push(createFakeFlights());
  }
  await knex('flights').insert(fakeFlights);

  // ROUTES TABLE
  const fakeRoutes = [];
  for (let i = 0; i < NUM; i++) {
    fakeRoutes.push(createFakeRoutes());
  }
  await knex('routes').insert(fakeRoutes);

  // BOOKINGS TABLE
  const fakeBookings = [];
  for (let i = 0; i < NUM; i++) {
    fakeBookings.push(createFakeBookings());
  }
  await knex('bookings').insert(fakeBookings);

  // CLASSES TABLE
  const fakeClasses = [];
  for (let i = 0; i < NUM; i++) {
    fakeClasses.push(createFakeClasses());
  }
  await knex('classes').insert(fakeClasses);

  // SEATS TABLE
  const fakeSeats = [];
  for (let i = 0; i < NUM; i++) {
    fakeSeats.push(createFakeSeats());
  }
  await knex('seats').insert(fakeSeats);

  // SEATS TABLE
  const fakeTickets = [];
  for (let i = 0; i < NUM; i++) {
    fakeTickets.push(createFakeTickets());
  }
  await knex('tickets').insert(fakeTickets);
};
