import { Request, Response } from 'express';
import knex from '../db/knex.js';

const get_all_users = async (req: Request, res: Response) => {
  try {
    const all_users = await knex.select().from('users');
    res.status(201).json({ success: true, all_users });
  } catch (err) {
    res.status(401).json({ success: false, err: err });
  }
};

const create_new_user = async (req: Request, res: Response) => {
  const { phonenumber, email, password } = req.body;
  const data = { phonenumber, email, password };
  try {
    await knex('users').insert(data);
    res.status(201).json({ success: true, new_user: 'new user created' });
  } catch (err) {
    res.status(401).json({ success: false, err: err });
  }
};

const update_user_by_id = async (req: Request, res: Response) => {
  const { userId } = req.params;
  const { phonenumber, email, password } = req.body;
  const data = { phonenumber, email, password };
  try {
    const updated_user = await knex('users').where('id', userId).update(data);
    res.status(201).json({ success: true, updated_user });
  } catch (err) {
    res.status(401).json({ success: false, err: err });
  }
};

const delete_user_by_id = async (req: Request, res: Response) => {
  const { userId } = req.params;
  try {
    const deleted_userId = await knex('users').where('id', userId).del();
    res
      .status(201)
      .json({ success: true, msg: 'user deleted', deleted_userId });
  } catch (err) {
    res.status(401).json({ success: false, err: err });
  }
};

export default {
  get_all_users,
  create_new_user,
  update_user_by_id,
  delete_user_by_id,
};
