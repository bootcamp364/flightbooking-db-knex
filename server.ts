import express from 'express';
import userRoute from './routes/users';

const port = process.env.PORT || 5000;

const app = express();
app.use(express.json());
app.use('/user', userRoute);

app.listen(port, () => {
  console.log('listening on port: ', port);
});
