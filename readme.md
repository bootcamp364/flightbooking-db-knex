# Flight Booking DB - Knex

[This](https://whimsical.com/er-flight-booking-UKXdppwQ9xBdHfTvD4txnm) is the link to ER Diagram.

Tasks Done

- configured knex
- created a migration file
- seeding data
- creating routes
- docker node js setup
- docker postgres knex setup

## Dockerfile for Node JS Porject

- build image => `docker build -t fbapp:1.0 .`
- run the docker image => `docker run -p 8001:8000 fbapp:3.0`

```docker
FROM node
WORKDIR /usr/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
WORKDIR /usr/app/dist
EXPOSE 8000
CMD [ "node", "server.js" ]
```

## Docker Postgres Knex Setup

- `docker run --name [some-postgres-container-name] -p 5432:5432 -e POSTGRES_PASSWORD=tani123 -d postgres`

<!-- SET UP -->

- `docker-compose -f docker-compose.yaml up`
- `docker ps`
- `docker exec flightbooking-db-knex-server-1 npm start`
- `docker exec flightbooking-db-knex-server-1 npm run migrate`

<!-- REMOVE -->

- `docker-compose -f docker-compose.yaml down`
- `docker images`
- `docker rmi flightbooking-db-knex-server`
